package gameworld

type Faction int

const (
	FactionBurningWall = 1
	FactionLunaki      = 2
	FactionRegium      = 3
	FactionElectus     = 4
)
